<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    /**
     * @throws \yii\base\Exception
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        /** Permissions for Workers */

        $listWorkers = $auth->createPermission('listWorkers');
        $listWorkers->description = 'View list of workers';
        $auth->add($listWorkers);

        $viewWorker = $auth->createPermission('viewWorker');
        $viewWorker->description = 'View a worker';
        $auth->add($viewWorker);

        $createWorker = $auth->createPermission('createWorker');
        $createWorker->description = 'Create worker';
        $auth->add($createWorker);

        $updateWorker = $auth->createPermission('updateWorker');
        $updateWorker->description = 'Update worker';
        $auth->add($updateWorker);

        /** Permissions for Movies */

        $listMovies = $auth->createPermission('listMovies');
        $listMovies->description = 'View list of movies';
        $auth->add($listMovies);

        $viewMovie = $auth->createPermission('viewMovie');
        $viewMovie->description = 'View a movie';
        $auth->add($viewMovie);

        $createMovie = $auth->createPermission('createMovie');
        $createMovie->description = 'Create movie';
        $auth->add($createMovie);

        $updateMovie = $auth->createPermission('updateMovie');
        $updateMovie->description = 'Update movie';
        $auth->add($updateMovie);

        /** Permissions for Halls */

        $listHalls = $auth->createPermission('listHalls');
        $listHalls->description = 'View list of halls';
        $auth->add($listHalls);

        $viewHall = $auth->createPermission('viewHall');
        $viewHall->description = 'View a hall';
        $auth->add($viewHall);

        $createHall = $auth->createPermission('createHall');
        $createHall->description = 'Create hall';
        $auth->add($createHall);

        $updateHall = $auth->createPermission('updateHall');
        $updateHall->description = 'Update hall';
        $auth->add($updateHall);

        /** Permissions for Shows */

        $listShows = $auth->createPermission('listShows');
        $listShows->description = 'View list of shows';
        $auth->add($listShows);

        $viewShow = $auth->createPermission('viewShow');
        $viewShow->description = 'View a show';
        $auth->add($viewShow);

        $createShow = $auth->createPermission('createShow');
        $createShow->description = 'Create show';
        $auth->add($createShow);

        $updateShow = $auth->createPermission('updateShow');
        $updateShow->description = 'Update show';
        $auth->add($updateShow);

        /** Roles */

        $worker = $auth->createRole('worker');
        $auth->add($worker);
        $auth->addChild($worker, $listMovies);
        $auth->addChild($worker, $viewMovie);
        $auth->addChild($worker, $createMovie);
        $auth->addChild($worker, $updateMovie);

        $auth->addChild($worker, $listHalls);
        $auth->addChild($worker, $viewHall);
        $auth->addChild($worker, $createHall);
        $auth->addChild($worker, $updateHall);

        $auth->addChild($worker, $listShows);
        $auth->addChild($worker, $viewShow);
        $auth->addChild($worker, $createShow);
        $auth->addChild($worker, $updateShow);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $listWorkers);
        $auth->addChild($admin, $viewWorker);
        $auth->addChild($admin, $createWorker);
        $auth->addChild($admin, $updateWorker);

        $auth->addChild($admin, $worker);
    }
}
