<?php

namespace app\commands;

use app\models\Worker;
use Yii;
use yii\console\Controller;

class AdminController extends Controller
{
    /**
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $worker = new Worker();
        $worker->username = 'admin';

        $worker->setPassword('password');
        $worker->generateAuthKey();

        $worker->lastName = 'admin';
        $worker->firstName = 'admin';
        $worker->dateOfBirth = '2000-01-01';

        if ($worker->save()) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole('admin');
            $auth->assign($role, $worker->getId());
        }
    }
}
