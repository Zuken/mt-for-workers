<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class MovieForm extends Model
{
    /** @var integer|null  */
    public $id;
    /** @var string */
    public $name;
    /** @var string */
    public $oldName;
    /** @var string[] */
    public $genre;
    /** @var string[] */
    public $country;
    /** @var integer */
    public $hours;
    /** @var integer */
    public $minutes;
    /** @var string */
    public $director;
    /** @var string|null */
    public $actors;
    /** @var string */
    public $release_date;
    /** @var string */
    public $description;
    /** @var integer */
    public $status;
    /** @var UploadedFile */
    public $poster;
    /** @var UploadedFile[] */
    public $movie_shots;
    /** @var UploadedFile */
    public $trailer;

    public function __construct($config = [])
    {
        parent::__construct($config);
        if($this->id != null) {
            $this->fillFields($this->id);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'director', 'description', 'actors'], 'trim'],
            [['name', 'genre', 'country', 'hours', 'minutes', 'director', 'release_date', 'description'], 'required'],
            [['poster', 'movie_shots', 'trailer'], 'required', 'on' => 'create'],
            [['description'], 'string'],
            [['name', 'director'], 'string', 'max' => 45],
            [['actors'], 'string', 'max' => 100],
            [['status', 'hours', 'minutes'], 'integer'],
            [['poster'], 'image', /*'skipOnEmpty' => false,*/ 'extensions' => 'jpg', 'checkExtensionByMimeType'=>false],
            [['trailer'], 'file', /*'skipOnEmpty' => false,*/ 'extensions' => 'mp4', 'checkExtensionByMimeType'=>false],
            [['movie_shots'], 'image', /*'skipOnEmpty' => false,*/ 'extensions' => 'jpg', 'maxFiles' => 10, 'checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'name' => 'Название',
            'genre' => 'Жанр',
            'country' => 'Страна',
            'hours' => 'Часы',
            'minutes' => 'Минуты',
            'director' => 'Режиссер',
            'actors' => 'В ролях',
            'release_date' => 'Дата выхода',
            'description' => 'Описание',
            'status' => 'Статус',
            'poster' => 'Постер',
            'movie_shots' => 'Кадры из фильма',
            'trailer' => 'Трейлер'
        ];
    }

    /**
     * @return Movie|bool|null
     * @throws \yii\base\Exception
     */
    public function create()
    {
        if (!$this->validate()) {
            return false;
        }
        $movie = self::getMovie();

        $moviePath = 'images/movies/' . $this->transliterate($this->name) . '/';

        if (!file_exists($moviePath)) {
            FileHelper::createDirectory($moviePath . 'movie_shots');
        }

        $this->poster->saveAs($moviePath . 'poster.' . $this->poster->extension);
        $this->trailer->saveAs($moviePath . 'trailer.' . $this->trailer->extension);

        $movie->images_path = $moviePath;

        foreach ($this->movie_shots as $image) {
            $skip = false;
            while (!$skip) {
                $filename = uniqid();
                $imagePath = $moviePath . 'movie_shots/' . $filename . '.' . $image->extension;
                if (!file_exists($imagePath)) {
                    $skip = true;
                    $image->saveAs($imagePath);
                }
            }
        }

        return $movie->save() ? $movie : false;
    }

    /**
     * @return Movie|bool|null
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     */
    public function update()
    {
        if (!$this->validate()) {
            return false;
        }

        $movie = self::getMovie();

        if ($this->name != $this->oldName) {
            $oldMoviePath = 'images/movies/' . $this->transliterate($this->oldName);
            $newMoviePath = 'images/movies/' . $this->transliterate($this->name);
            FileHelper::copyDirectory($oldMoviePath, $newMoviePath);
            FileHelper::removeDirectory($oldMoviePath);
        }

        if (!is_null($this->poster) || !is_null($this->movie_shots) || !is_null($this->trailer)) {
            $moviePath = 'images/movies/' . $this->transliterate($this->name) . '/';

            if (!file_exists($moviePath)) {
                FileHelper::createDirectory($moviePath . 'movie_shots');
            }
            if (!is_null($this->poster)) {
                $this->poster->saveAs($moviePath . 'poster.' . $this->poster->extension);
            }
            if (!is_null($this->trailer)) {
                $this->trailer->saveAs($moviePath . 'trailer.' . $this->trailer->extension);
            }
            $movie->images_path = $moviePath;

            if (!is_null($this->movie_shots)) {

                foreach ($this->movie_shots as $image) {
                    $skip = false;
                    while (!$skip) {
                        $filename = uniqid();
                        $imagePath = $moviePath . 'movie_shots/' . $filename . '.' . $image->extension;
                        if (!file_exists($imagePath)) {
                            $skip = true;
                            $image->saveAs($imagePath);
                        }
                    }
                }
            }
        }
        return $movie->save() ? $movie : false;
    }

    public function getPosterUrl()
    {
        return '/images/movies/' . $this->transliterate($this->name) . '/poster.jpg';
    }

    public function getMovieShotsUrl()
    {
        if (empty($this->name)) {
            return [];
        }
        $path = 'images/movies/' . $this->transliterate($this->name) . '/movie_shots';
        $files = FileHelper::findFiles($path);
        foreach ($files as $index => $file) {
            $files[$index] = '/' . $file;
        }

        return $files;
    }

    public function getPreviewConfig()
    {
        if (empty($this->name)) {
            return [];
        }
        $path = 'images/movies/' . $this->transliterate($this->name) . '/movie_shots';
        $files = FileHelper::findFiles($path);
        $result = [];
        foreach ($files as $file) {
            array_push($result, ['key' => $file]);
        }
        return $result;
    }

    public function getTrailerUrl()
    {
        return '/images/movies/' . $this->transliterate($this->name) . '/trailer.mp4';
    }

    /**
     * @return Movie|null
     */
    private function getMovie()
    {
        $movie = new Movie();
        if ($this->id) {
            $movie = Movie::findOne($this->id);
        }

        $movie->name = $this->name;

        $genres = '';
        foreach ($this->genre as $key => $genre) {
            $genres .= ($key == 0 ? '' : ', ') . $genre;
        }
        $movie->genre = $genres;

        $countries = '';
        foreach ($this->country as $key => $country) {
            $countries .= ($key == 0 ? '' : ', ') . $country;
        }
        $movie->country = $countries;

        if ($this->minutes < 10) {
            $this->minutes = '0' . $this->minutes;
        }

        $movie->duration = $this->hours . ':' . $this->minutes;
        $movie->director = $this->director;
        $movie->actors = $this->actors;
        $movie->release_date = $this->release_date;
        $movie->description = $this->description;
        $movie->status = $this->status;

        return $movie;
    }

    /**
     * Заполняет форму значениям из БД
     *
     * @param $id
     * @return $this
     */
    private function fillFields($id)
    {
        $movie = Movie::findOne($id);
        $this->name = $movie->name;
        $this->oldName = $movie->name;
        $this->genre = explode(', ', $movie->genre);
        $this->country = $movie->country;

        $duration = explode(':', $movie->duration);

        $this->hours = $duration[0];
        $this->minutes = $duration[1];
        $this->director = $movie->director;
        $this->description = $movie->description;
        $this->actors = $movie->actors;
        $this->release_date = $movie->release_date;
        $this->status = $movie->status;

        return $this;
    }

    private function transliterate($string) {
        $string = mb_strtolower($string);
        $converter  = array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ъ' => 'y', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', ' ' => '-');
        return strtr($string, $converter);
    }
}
