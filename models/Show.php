<?php

namespace app\models;

/**
 * This is the model class for table "shows".
 *
 * @property int $id
 * @property int $movie_id
 * @property int $hall_id
 * @property string $date
 * @property string $time
 * @property int $price
 * @property int $status
 *
 * @property Hall $hall
 * @property Movie $movie
 * @property Tickets[] $tickets
 */
class Show extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shows';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['movie_id', 'hall_id', 'date', 'time', 'price'], 'required'],
            [['movie_id', 'hall_id', 'price', 'status'], 'integer'],
            [['date', 'time'], 'safe'],
            [['hall_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hall::class, 'targetAttribute' => ['hall_id' => 'id']],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movie::class, 'targetAttribute' => ['movie_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'movie_id' => 'Фильм',
            'movieName' => 'Название фильма',
            'hall_id' => 'Зал',
            'hallNumber' => 'Номер зала',
            'date' => 'Дата',
            'time' => 'Время',
            'price' => 'Цена',
            'status' => 'Статус',
        ];
    }

    public function getMovieName()
    {
        return $this->movie->name;
    }

    public function getHallNumber()
    {
        return $this->hall->number;
    }

    /**
     * Gets query for Hall.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHall()
    {
        return $this->hasOne(Hall::class, ['id' => 'hall_id']);
    }

    /**
     * Gets query for Movie.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMovie()
    {
        return $this->hasOne(Movie::class, ['id' => 'movie_id']);
    }

    /**
     * Gets query for Tickets.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Tickets::class, ['show_id' => 'id']);
    }

    /**
     * Возвращает кол-во записей
     *
     * @param array $params
     * @return int|string
     */
    public static function getCount($params = [])
    {
        return static::find()->where($params)->count();
    }
}
