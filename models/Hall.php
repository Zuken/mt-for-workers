<?php

namespace app\models;

/**
 * This is the model class for table "halls".
 *
 * @property int $id
 * @property int $hall_layout_id
 * @property int $number
 * @property int $status
 *
 * @property HallLayout $layout
 * @property Show[] $shows
 */
class Hall extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'halls';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['number', 'status', 'hall_layout_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'number' => 'Номер зала',
            'status' => 'Статус',
            'hall_layout_id' => 'Схема'
        ];
    }

    /**
     * Gets query for Layouts.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLayout()
    {
        return $this->hasOne(HallLayout::class, ['id' => 'hall_layout_id']);
    }

    /**
     * Gets query for Shows.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShows()
    {
        return $this->hasMany(Show::class, ['hall_id' => 'id']);
    }

    /**
     * Возвращает кол-во записей
     *
     * @param array $params
     * @return int|string
     */
    public static function getCount($params = [])
    {
        return static::find()->where($params)->count();
    }
}
