<?php

namespace app\models;

use Yii;
use yii\base\Model;

class WorkerForm extends Model
{
    /** @var integer */
    public $id;
    /** @var string */
    public $username;
    /** @var string */
    public $password;
    /** @var string */
    public $repeatPassword;
    /** @var integer */
    public $status;
    /** @var string */
    public $lastName;
    /** @var string */
    public $firstName;
    /** @var string */
    public $middleName;
    /** @var string */
    public $dateOfBirth;
    /** @var string */
    public $role;

    public function __construct($config = [])
    {
        parent::__construct($config);
        if($this->id != null) {
            $this->fillFields($this->id);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'lastName', 'firstName', 'middleName'], 'trim'],
            [['username', 'status', 'lastName', 'firstName', 'dateOfBirth', 'role'], 'required'],
            [['middleName'], 'safe'],
            [['dateOfBirth', 'role'], 'string'],
            [['username', 'middleName'], 'string', 'min' => 5, 'max' => 20],
            [['lastName', 'firstName'], 'string', 'min' => 2, 'max' => 20],
            [['password', 'repeatPassword'], 'required', 'on' => 'create'],
            [['password', 'repeatPassword'], 'safe', 'on' => 'update'],
            [['password', 'repeatPassword'], 'string', 'min' => 6, 'max' => 20],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password' , 'skipOnEmpty' => false],
            [['status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль',
            'status' => 'Статус',
            'lastName' => 'Фамилия',
            'firstName' => 'Имя',
            'middleName' => 'Отчество',
            'dateOfBirth' => 'Дата рождения',
            'role' => 'Роль'
        ];
    }

    /**
     * Добавляет новую запись Сотрудника в БД
     *
     * @return \app\models\Worker|bool
     * @throws \yii\base\Exception
     */
    public function create()
    {
        if (!$this->validate()) {
            return false;
        }
        $worker = new Worker();

        $worker = $this->setValuesWorker($worker);

        $worker->username = $this->username;
        $worker->setPassword($this->password);
        $worker->generateAuthKey();

        if ($worker->save()) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($this->role);
            $auth->assign($role, $worker->getId());
            return $worker;
        }
        return false;
    }

    /**
     * Добавляет новую запись Сотрудника в БД
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function update()
    {
        if (!$this->validate()) {
            return false;
        }
        $worker = Worker::findOne($this->id);

        $worker = $this->setValuesWorker($worker);

        if ($this->username != $worker->username) {
            $worker->username = $this->username;
        }

        if (!empty($this->password) && !$worker->validatePassword($this->password)) {
            $worker->setPassword($this->password);
        }

        if ($worker->save()) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($this->role);
            $auth->revokeAll($worker->getId());
            $auth->assign($role, $worker->getId());
            return true;
        }
        return false;
    }

    /**
     * Заполняет форму значениям из БД
     *
     * @param $id
     * @return $this
     */
    private function fillFields($id)
    {
        $worker = Worker::findOne($id);
        $this->username = $worker->username;
        $this->status = $worker->status;

        $this->role = array_key_first(Yii::$app->authManager->getRolesByUser($worker->getId()));

        $this->lastName = $worker->lastName;
        $this->firstName = $worker->firstName;
        $this->middleName = $worker->middleName;
        $this->dateOfBirth = $worker->dateOfBirth;

        return $this;
    }

    /**
     * @param $worker Worker
     * @return Worker
     */
    private function setValuesWorker($worker)
    {
        $worker->status = $this->status;

        $worker->lastName = $this->lastName;
        $worker->firstName = $this->firstName;
        $worker->middleName = $this->middleName;
        $worker->dateOfBirth = $this->dateOfBirth;

        return $worker;
    }
}
