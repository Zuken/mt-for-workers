<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * WorkerSearch represents the model behind the search form of `app\models\Worker`.
 */
class WorkerSearch extends Worker
{
    /** @var string */
    public $fullName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['username', 'auth_key', 'password', 'fullName', 'dateOfBirth'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Worker::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
            'attributes' => [
                'id',
                'fullName' => [
                    'asc' => ['lastName' => SORT_ASC, 'firstName' => SORT_ASC, 'middleName' => SORT_ASC],
                    'desc' => ['lastName' => SORT_DESC, 'firstName' => SORT_DESC, 'middleName' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'dateOfBirth',
                'username',
                'status',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'dateOfBirth' => $this->dateOfBirth,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password', $this->password]);

        $fullName = explode(' ', $this->fullName);

        if (count($fullName) == 1) {
            $query->andWhere("lastName LIKE '$fullName[0]%' OR firstName LIKE '$fullName[0]%' OR middleName LIKE '$fullName[0]%'");
        } elseif (count($fullName) == 2) {
            $query->andWhere("(lastName LIKE '$fullName[0]%' OR lastName LIKE '$fullName[1]%' OR middleName LIKE '$fullName[1]%') AND (firstName LIKE '$fullName[0]%' OR firstName LIKE '$fullName[1]%')");
        } elseif (count($fullName) == 3) {
            $query->andWhere("lastName LIKE '$fullName[0]%' AND firstName LIKE '$fullName[1]%' AND middleName LIKE '$fullName[2]%'");
        }

        return $dataProvider;
    }
}
