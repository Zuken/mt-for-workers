<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ShowSearch represents the model behind the search form of `app\models\Show`.
 */
class ShowSearch extends Show
{
    /** @var string */
    public $movieName;
    /** @var integer */
    public $hallNumber;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'hallNumber', 'price', 'status'], 'integer'],
            [['date', 'time', 'movieName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Show::find()->joinWith("movie")->joinWith("hall");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
            'attributes' => [
                'id',
                'movieName' => [
                    'asc' => ['movies.name' => SORT_ASC],
                    'desc' => ['movies.name' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'hallNumber' => [
                    'asc' => ['halls.number' => SORT_ASC],
                    'desc' => ['halls.number' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'date',
                'time',
                'price',
                'status',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'hallNumber' => $this->hallNumber,
            'date' => $this->date,
            'time' => $this->time,
            'price' => $this->price,
            'shows.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'movies.name', $this->movieName]);

        return $dataProvider;
    }
}
