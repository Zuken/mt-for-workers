<?php

namespace app\models;

/**
 * This is the model class for table "hall_layouts".
 *
 * @property int $id
 * @property int $layout
 * @property int rows
 * @property int seats
 * @property string offSeats
 *
 * @property Hall[] $halls
 */
class HallLayout extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hall_layouts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['layout', 'rows', 'seats'], 'required'],
            [['rows', 'seats'], 'integer'],
            [['offSeats', 'layout'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'layout' => 'Layout',
            'rows' => 'Rows',
            'seats' => 'Seats',
            'offSeats' => 'Off seats'
        ];
    }

    /**
     * Gets query for Halls.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHalls()
    {
        return $this->hasMany(Hall::class, ['hall_layout_id' => 'id']);
    }
}
