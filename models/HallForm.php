<?php

namespace app\models;

use yii\base\Model;

class HallForm extends Model
{
    /** @var int */
    public $id;
    /** @var int */
    public $layoutId;
    /** @var int */
    public $number;
    /** @var  int*/
    public $status;
    /** @var bool */
    public $newSchema;
    /** @var int */
    public $rows;
    /** @var int */
    public $seats;
    /** @var int */
    public $offSeats;
    /** @var string */
    public $name;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        if ($this->id) {
            $this->FillFields($this->id);
        }
    }

    public function rules()
    {
        return [
            ['name', 'trim'],
            [['number', 'status', 'newSchema'], 'required'],

            [['name', 'rows', 'seats'], 'required', 'when' => function ($model) {
                return $model->newSchema == 1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#select-schema-type').val() == '1';
            }"],
            [['layoutId'], 'required', 'when' => function ($model) {
                return $model->newSchema == 0;
            }, 'whenClient' => "function (attribute, value) {
                return $('#select-schema-type').val() == '0';
            }"],

            [['number', 'status', 'rows', 'seats', 'layoutId', 'id'], 'integer'],
            ['offSeats', 'string'],
            [['name'], 'string', 'max' => 30],
            ['newSchema', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'number' => 'Номер зала',
            'status' => 'Статус',
            'rows' => 'Кол-во рядов',
            'seats' => 'Кол-во мест в ряду',
            'newSchema' => 'Схема',
            'name' => 'Название схемы',
            'layoutId' => 'Схема зала',
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $layout = new HallLayout();

        if ($this->newSchema) {
            $layout->layout = $this->name;
            $layout->rows = $this->rows;
            $layout->seats = $this->seats;
            $layout->offSeats = $this->offSeats;
        } else {
            $layout = HallLayout::findOne($this->layoutId);
        }

        if (!$layout->save()) {
            return false;
        }

        $hall = new Hall();
        if ($this->id) {
            $hall = Hall::findOne($this->id);
        }

        $hall->hall_layout_id = $layout->id;
        $hall->number = $this->number;
        $hall->status = $this->status;

        return $hall->save() ? true : false;
    }

    public function FillFields($id)
    {
        $hall = Hall::findOne($id);

        $this->layoutId = $hall->hall_layout_id;
        $this->number = $hall->number;
        $this->status = $hall->status;

        $this->newSchema = 0;
    }
}
