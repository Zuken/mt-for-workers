<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "workers".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property int $status
 * @property string $lastName
 * @property string $firstName
 * @property string $middleName
 * @property string $dateOfBirth
 */
class Worker extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'workers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password', 'lastName', 'firstName', 'dateOfBirth'], 'required'],
            [['middleName'], 'safe'],
            [['status'], 'integer'],
            ['dateOfBirth', 'string'],
            [['username', 'lastName', 'firstName', 'middleName'], 'string', 'max' => 20],
            [['auth_key'], 'string', 'max' => 32],
            [['password'], 'string', 'max' => 256],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'username' => 'Имя пользователя',
            'auth_key' => 'Auth Key',
            'password' => 'Пароль',
            'status' => 'Статус',
            'fullName' => 'ФИО',
            'dateOfBirth' => 'Дата рождения'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return null|void|IdentityInterface
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Возвращает ФИО
     * @return string
     */
    public function getFullName()
    {
        return $this->lastName . ' ' . $this->firstName . ' ' . $this->middleName;
    }

    /**
     * Sets a hashed password
     *
     * @param $password string
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Генерирует токен
     *
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Возвращает кол-во записей
     *
     * @param array $params
     * @return int|string
     */
    public static function getCount($params = [])
    {
        return static::find()->where($params)->count();
    }
}
