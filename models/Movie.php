<?php

namespace app\models;

/**
 * This is the model class for table "movies".
 *
 * @property int $id
 * @property string $name
 * @property string $genre
 * @property string $country
 * @property string $duration
 * @property string $director
 * @property string|null $actors
 * @property string $release_date
 * @property string $description
 * @property int $status
 * @property string $images_path
 *
 * @property Show[] $shows
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'genre', 'country', 'duration', 'director', 'release_date', 'description', 'status', 'images_path'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['name', 'director'], 'string', 'max' => 45],
            [['genre', 'actors', 'images_path'], 'string', 'max' => 100],
            [['duration'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'name' => 'Название',
            'genre' => 'Жанр',
            'country' => 'Страна',
            'duration' => 'Длительность',
            'director' => 'Режиссер',
            'actors' => 'В ролях',
            'release_date' => 'Дата выхода',
            'description' => 'Описание',
            'status' => 'Статус',
            'images_path' => 'Путь к изображениям',
        ];
    }

    /**
     * Gets query for Shows.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShows()
    {
        return $this->hasMany(Show::class, ['movie_id' => 'id']);
    }

    /**
     * Возвращает кол-во записей
     *
     * @param array $params
     * @return int|string
     */
    public static function getCount($params = [])
    {
        return static::find()->where($params)->count();
    }
}
