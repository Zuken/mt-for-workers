<?php
return [
    '/' => 'site/index',
    '<action:(login|logout)>' => 'site/<action>',

    'workers' => 'worker/index',
    'worker/create' => 'worker/create',
    'worker/<id:\d+>' => 'worker/view',
    'worker/<id:\d+>/update' => 'worker/update',

    'halls' => 'hall/index',
    'hall/create' => 'hall/create',
    'hall/<id:\d+>' => 'hall/view',
    'hall/<id:\d+>/update' => 'hall/update',
    'hall/schema' => 'hall/schema',

    'movies' => 'movie/index',
    'movie/create' => 'movie/create',
    'movie/<id:\d+>' => 'movie/view',
    'movie/<id:\d+>/update' => 'movie/update',

    'shows' => 'show/index',
    'show/create' => 'show/create',
    'show/<id:\d+>' => 'show/view',
    'show/<id:\d+>/update' => 'show/update',

    'POST movie/delete' => 'movie/delete',

    'users' => 'users/index',
    'user/create' => 'users/create',
    'user/<id:\d+>' => 'users/view',
    'user/<id:\d+>/update' => 'users/update',

    'tickets' => 'tickets/index',
    'ticket/create' => 'tickets/create',
    'ticket/<id:\d+>' => 'tickets/view',
    'ticket/<id:\d+>/update' => 'tickets/update',
];
