<?php

namespace app\controllers;

use Yii;
use app\models\MovieForm;
use app\models\Movie;
use app\models\MovieSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * MovieController implements the CRUD actions for Movie model.
 */
class MovieController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Movie models.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('listMovies')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $searchModel = new MovieSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Movie model.
     * @param integer $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewMovie')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('createMovie')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $model = new MovieForm(['scenario' => 'create']);

        if ($model->load(Yii::$app->request->post())) {
            $model->poster = UploadedFile::getInstance($model, 'poster');
            $model->movie_shots = UploadedFile::getInstances($model, 'movie_shots');
            $model->trailer = UploadedFile::getInstance($model, 'trailer');

            if ($movie = $model->create()) {
                Yii::$app->session->setFlash('success', 'Запись сохранена');
                return $this->redirect(['view', 'id' => $movie->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Movie model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateMovie')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $model = new MovieForm(['id' => $id]);

        if ($model->load(Yii::$app->request->post())) {
            $model->poster = UploadedFile::getInstance($model, 'poster');
            $model->movie_shots = UploadedFile::getInstances($model, 'movie_shots');
            $model->trailer = UploadedFile::getInstance($model, 'trailer');

            if ($model->update()) {
                Yii::$app->session->setFlash('success', 'Запись сохранена');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete()
    {
        $filePath = Yii::$app->request->post('key');
        unlink($filePath);
        return '{}';
    }

    /**
     * Finds the Movie model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Movie the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Movie::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
