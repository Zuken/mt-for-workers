
"use strict";

$(document).ready(function(){

    $(document).on('click', '#btn-generate-schema', function () {
        $('#schema').html('');
        let rows = Number($('#input-rows').val());
        let seats = Number($('#input-seats').val());

        if (rows && seats)
        {
            for (let i = 1; i < rows + 1; i++) {

                let rowDiv = $('<div>', { class: "row" }).appendTo('#schema');
                let rowLabel = $('<div>', { class: "row-label col-lg-1" }).appendTo(rowDiv);
                $('<span>', { text: i + " ряд" }).appendTo(rowLabel);
                let seatsDiv = $('<div>', { class: "col-lg-11" }).appendTo(rowDiv);

                for (let j = 1; j < seats + 1; j++) {
                    $('<button>', {class : 'btn-seat', type: 'button', value : i + "," + j, text: j}).appendTo(seatsDiv);
                }
            }
        }

    });

    $(document).on('click', '.btn-seat', function () {
        if ($('#select-schema-type').val() == 1) {
            let seat = $(this).val();
            let offSeats = $('#input-off-seats').val();

            if (this.classList.contains("off")) {
                this.classList.remove("off");
                let newValue = offSeats.replace(seat + ";", "");
                $('#input-off-seats').val(newValue);

            } else {
                this.classList.add("off");
                let  newValue = offSeats + seat + ";";
                $('#input-off-seats').val(newValue);
            }
        }
    });

    $(document).on('change', '#select-schema-type', function () {
        $('#schema').html('');

        if ($(this).val() == 1) {
            $('#new-schema').show();
            $('#exists-schema').hide();
        } else {
            $('#new-schema').hide();
            $('#exists-schema').show();
        }
    });

    $('#select-schema-type').trigger('change');

    $(document).on('change', '#hallform-layoutid', function () {
        let selected = $(this).val();

        if (selected) {
            $.ajax({
                url: '/hall/schema',
                type: 'POST',
                data: {
                    id: selected
                },
                success: function(data) {
                    $('#schema').html(data);
                },
            });
        }
    });

    if ($('#select-schema-type').val() == 0) {
        if ($('#hallform-layoutid').val()) {
            $('#hallform-layoutid').trigger('change');
        }
    }
});
