<?php

use yii\db\Migration;

/**
 * Handles the creation of table `workers`.
 */
class m200325_102840_create_workers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('workers', [
            'id' => $this->primaryKey(),
            'username' => $this->string(20)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password' => $this->string(256)->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue('0'),
            'lastName' => $this->string(20)->notNull(),
            'firstName' => $this->string(20)->notNull(),
            'middleName' => $this->string(20),
            'dateOfBirth' => $this->date()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('workers');
    }
}
