<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shows`.
 */
class m200506_084447_create_shows_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('shows', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'hall_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'time' => $this->time()->notNull(),
            'price' => $this->integer(5)->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'FK_Shows_Movies',
            'shows',
            'movie_id',
            'movies',
            'id'
        );

        $this->addForeignKey(
            'FK_Shows_Halls',
            'shows',
            'hall_id',
            'halls',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_Shows_Halls', 'shows');
        $this->dropForeignKey('FK_Shows_Movies', 'shows');
        $this->dropTable('shows');
    }
}
