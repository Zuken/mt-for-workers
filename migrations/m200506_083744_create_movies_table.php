<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movies`.
 */
class m200506_083744_create_movies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('movies', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'genre' => $this->string(100)->notNull(),
            'country' => $this->string(45)->notNull(),
            'duration' => $this->string(5)->notNull(),
            'director' => $this->string(45)->notNull(),
            'actors' => $this->string(100),
            'release_date' => $this->date()->notNull(),
            'description' => $this->string(3000)->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'images_path' => $this->string(100)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('movies');
    }
}
