<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hall_layouts`.
 */
class m200324_100125_create_hall_layouts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('hall_layouts', [
            'id' => $this->primaryKey(),
            'layout' => $this->string(30)->notNull(),
            'rows' => $this->tinyInteger(2)->notNull(),
            'seats' => $this->tinyInteger(2)->notNull(),
            'offSeats' => $this->string(100)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('hall_layouts');
    }
}
