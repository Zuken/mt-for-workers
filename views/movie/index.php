<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MovieSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фильмы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movie-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить фильм', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'movies', 'enablePushState' => false]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'genre',
                'country',
                'duration',
                [
                    'attribute' => 'release_date',
                    'value' => function ($movie) {

                        return Yii::$app->formatter->asDate($movie->release_date, 'dd/MM/yyyy');
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'readonly' => true,
                        'attribute' => 'release_date',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ]
                    ]),
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($show) {
                        if ($show->status == 0) {
                            return 'Скоро в прокате';
                        } elseif ($show->status == 1) {
                            return 'В прокате';
                        }
                        return 'Прокат завершен';
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        [
                            '0' => 'Скоро в прокате',
                            '1' => 'В прокате',
                            '2' => 'Прокат завершен'
                        ],
                        [
                            'class' => 'form-control',
                            'prompt' => 'Выбрать'
                        ]
                    ),
                    'headerOptions' => ['width' => '140'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{view} &nbsp {update}',
                    'headerOptions' => ['width' => '80'],
                ],
            ],
        ]); ?>

    <?php Pjax::end(); ?>

</div>
