<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Movie */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Фильмы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="movie-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'genre',
            'country',
            'duration',
            'director',
            'actors',
            'release_date',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => function ($movie) {
                    if ($movie->status == 0) {
                        return 'Скоро в прокате';
                    } elseif ($movie->status == 1) {
                        return 'В прокате';
                    }
                    return 'Завершен';
                },
            ],
            [
                'label' => 'Постер',
                'value' => function ($movie) {
                    return Html::img("/{$movie->images_path}poster.jpg", ['width' => '200px']);
                },
                'format' => 'html',
            ],
            [
                'label' => 'Кадры из фильма',
                'value' => function ($movie) {
                    $path = $movie->images_path . 'movie_shots';
                    $images = \yii\helpers\FileHelper::findFiles($path);
                    $result = "";
                    foreach ($images as $image) {
                        $result .= "<img src=\"/$image\" style=\"width: 400px;\">";
                    }
                    return $result;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Трейлер',
                'value' => function ($movie) {
                    return '<video controls style="width: 400px;"><source src="/' . $movie->images_path . 'trailer.mp4" type="video/mp4"></video>';
                },
                'format' => 'raw',
            ]
        ],
    ]) ?>
</div>
