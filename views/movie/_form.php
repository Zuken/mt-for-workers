<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\file\FileInput;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model app\models\MovieForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $isUpdate boolean */

if (!isset($isUpdate)) {
    $isUpdate = false;
}
?>

<div class="movie-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oldName')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'genre')->widget(Select2::class, [
        'name' => 'genre',
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Genre::find()->all(), 'name', 'name'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['placeholder' => 'Выберите жанр', 'multiple' => true, 'autocomplete' => 'off'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'country')->widget(Select2::class, [
        'name' => 'country',
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->all(), 'name', 'name'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['placeholder' => 'Выберите страну', 'multiple' => true, 'autocomplete' => 'off'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'hours')->widget(TouchSpin::class, [
        'pluginOptions' => [
            'verticalbuttons' => true,
            'min' => 0,
            'max' => 23,
        ],
    ]) ?>

    <?= $form->field($model, 'minutes')->widget(TouchSpin::class, [
        'pluginOptions' => [
            'verticalbuttons' => true,
            'min' => 0,
            'max' => 59,
        ],
    ]) ?>

    <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'actors')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'release_date')->widget(DatePicker::class, [
        'name' => 'release_date',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'readonly' => true,
        'pluginOptions' => [
            'todayHighlight' => true,
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Скоро в прокате',
        '1' => 'В прокате',
        '2' => 'Завершен'
    ]) ?>

    <?= $form->field($model, 'poster')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview' => $isUpdate ? $model->getPosterUrl() : '',
            'initialPreviewAsData' => true,
            'showUpload' => false,
            'allowedFileTypes' => ['image']
        ],
    ]) ?>

    <?= $form->field($model, 'movie_shots[]')->widget(FileInput::class, [
        'options' => [
            'multiple' => true,
            'accept' => 'image/*'
        ],
        'pluginOptions' => [
            'initialPreview' => $isUpdate ? $model->getMovieShotsUrl() : [],
            'initialPreviewAsData' => true,
            'initialPreviewConfig' => $isUpdate ? $model->getPreviewConfig() : [],
            'overwriteInitial' => false,
            'showUpload' => false,
            'deleteUrl' => \yii\helpers\Url::to(['movie/delete']),
            'allowedFileTypes' => ['image']
        ],
    ]) ?>

    <?= $form->field($model, 'trailer')->widget(FileInput::class, [
        'options' => ['accept' => 'video/*'],
        'pluginOptions' => [
            'initialPreview' => $isUpdate ? $model->getTrailerUrl() : '',
            'initialPreviewFileType'=> 'video',
            'initialPreviewAsData' => true,
            'initialPreviewConfig'=> [
                ['filetype'=> "video/mp4"],
                ['caption'=> $model->name ? $model->name : '']
            ],
            'allowedFileTypes' => ['video'],
            'allowedFileExtensions'=>['mp4'],
            'showUpload' => false
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
