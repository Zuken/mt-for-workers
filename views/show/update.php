<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Show */

$this->title = 'Редактирование: киносеанс №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Киносеансы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="show-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
