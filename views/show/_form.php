<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Show */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="show-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'movie_id')->widget(Select2::className(), [
        'name' => 'movie_id',
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Movie::find()->where(['status' => '1'])->all(), 'id', 'name'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['placeholder' => 'Выберите фильм', 'autocomplete' => 'off'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'hall_id')->widget(Select2::className(), [
        'name' => 'hall_id',
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Hall::find()->where(['status' => '0'])->all(), 'id', 'number'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['placeholder' => 'Выберите зал', 'autocomplete' => 'off'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::className(), [
        'name' => 'date',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'readonly' => true,
        'pluginOptions' => [
            'todayHighlight' => true,
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'time')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Скоро начнётся',
        '1' => 'Идёт',
        '2' => 'Завершен'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
