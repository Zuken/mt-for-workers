<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Киносеансы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="show-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить киносеанс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'shows', 'enablePushState' => false]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'headerOptions' => ['width' => '150']
                ],
                'movieName',
                'hallNumber',
                [
                    'attribute' => 'date',
                    'value' => function ($show) {

                        return Yii::$app->formatter->asDate($show->date, 'dd/MM/yyyy');
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'readonly' => true,
                        'attribute' => 'date',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ]
                    ]),
                ],
                [
                    'attribute' => 'time',
                    'value' => function ($show) {
                        return Yii::$app->formatter->asTime($show->time, 'short');
                    }
                ],
                'price',

                [
                    'attribute' => 'status',
                    'value' => function ($show) {
                        if ($show->status == 0) {
                            return 'Скоро';
                        } elseif ($show->status == 1) {
                            return 'Идёт';
                        }
                        return 'Завершен';
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        [
                            '0' => 'Скоро',
                            '1' => 'Идёт',
                            '2' => 'Завершен'
                        ],
                        [
                            'class' => 'form-control',
                            'prompt' => 'Выбрать'
                        ]
                    ),
                    'headerOptions' => ['width' => '135px'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{view} &nbsp {update}',
                    'headerOptions' => ['width' => '80'],
                ],
            ],
        ]); ?>

    <?php Pjax::end(); ?>

</div>
