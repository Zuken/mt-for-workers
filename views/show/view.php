<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Show */

$this->title = 'Киносеанс №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Киносеансы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="show-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'movieName',
            'hallNumber',
            [
                'attribute' => 'date',
                'value' => function($show) {
                    return Yii::$app->formatter->asDate($show->date, 'dd/MM/yyyy');
                },
            ],
            [
                'attribute' => 'time',
                'value' => function($show) {
                    return Yii::$app->formatter->asTime($show->time, 'short');
                },
            ],
            'price',
            [
                'attribute' => 'status',
                'value' => function ($show) {
                    if ($show->status == 0) {
                        return 'Скоро';
                    } elseif ($show->status == 1) {
                        return 'Идёт';
                    }
                    return 'Завершен';
                },
            ],
        ],
    ]) ?>

</div>
