<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="worker-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить сотрудника', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'workers', 'enablePushState' => false]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'headerOptions' => ['width' => '150']
                ],
                'fullName',
                [
                    'attribute' => 'dateOfBirth',
                    'value' => function ($worker) {

                        return Yii::$app->formatter->asDate($worker->dateOfBirth, 'dd/MM/yyyy');
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'readonly' => true,
                        'attribute' => 'dateOfBirth',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ]
                    ]),
                    'headerOptions' => ['width' => '210'],
                ],
                [
                    'attribute' => 'username',
                    'headerOptions' => ['width' => '150'],
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($hall) {
                        if ($hall->status == 0) {
                            return 'Активный';
                        } elseif ($hall->status == 1) {
                            return 'Отключен';
                        }
                        return 'Удален';
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        [
                            '0' => 'Активный',
                            '1' => 'Отключен',
                            '2' => 'Удален'
                        ],
                        [
                            'class' => 'form-control',
                            'prompt' => 'Выбрать'
                        ]
                    ),
                    'headerOptions' => ['width' => '130'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{view} &nbsp {update}',
                    'headerOptions' => ['width' => '80'],
                ],
            ],
        ]); ?>

    <?php Pjax::end(); ?>

</div>
