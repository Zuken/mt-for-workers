<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Worker */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Сотрудники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="worker-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            [
                'attribute' => 'status',
                'value' => function ($hall) {
                    if ($hall->status == 0) {
                        return 'Активный';
                    } elseif ($hall->status == 1) {
                        return 'Отключен';
                    }
                    return 'Удален';
                },
            ],
            [
                'attribute' => 'dateOfBirth',
                'value' => function($worker) {
                    return Yii::$app->formatter->asDate($worker->dateOfBirth, 'dd/MM/yyyy');
                }
            ],
            [
                'label' => 'Роль',
                'value' => function($worker) {
                    $role = array_key_first(Yii::$app->authManager->getRolesByUser($worker->id));
                    if ($role == 'worker') {
                        return 'Сотрудник';
                    }
                    return 'Администратор';
                },
            ],
        ],
    ]) ?>

</div>
