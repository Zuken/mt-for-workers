<?php

use yii\helpers\Html;

/* @var $layout app\models\HallLayout */

$offSeats = explode(';', $layout->offSeats);

for ($i = 1; $i < $layout->rows + 1; $i++) :
    ?>
    <div class="row" style="margin: 10px 0;">
        <div class="row-label col-lg-1" style="line-height: 40px;">
            <span ><?= "$i ряд" ?></span>
        </div>
        <div class="col-lg-11">
            <?php
            for ($j = 1; $j < $layout->seats + 1; $j++) {
                $seat = "$i,$j";
                $skip = false;
                foreach ($offSeats as $key => $offSeat) {
                    if ($offSeat == $seat) {

                        echo Html::button($j, ['class' => 'btn-seat', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px; opacity: 0;']);
                        unset($offSeats[$key]);
                        $skip = true;
                        break;
                    }
                }
                if (!$skip) {
                    echo Html::button($j, ['class' => 'btn-seat', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px;']);
                }
            }
            ?>
        </div>
    </div>
<?php endfor; ?>
