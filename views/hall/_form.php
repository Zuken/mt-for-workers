<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Hall */
/* @var $form yii\widgets\ActiveForm */

\app\assets\SchemaAsset::register($this);
?>

<div class="hall-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Активный',
        '1' => 'Отключен',
        '2' => 'Удален'
    ]) ?>

    <?= $form->field($model, 'offSeats')->hiddenInput(['id' => 'input-off-seats'])->label(false) ?>

    <?= $form->field($model, 'newSchema')->dropDownList(['1' => 'Создать новую', '0' => 'Выбрать из существующих'], ['id' => 'select-schema-type']) ?>

    <div id="new-schema">

        <?= $form->field($model, 'name')->textInput(['id' => 'input-name']) ?>

        <?= $form->field($model, 'rows')->textInput(['id' => 'input-rows']) ?>

        <?= $form->field($model, 'seats')->textInput(['id' => 'input-seats']) ?>

        <?= Html::button('Сгенерировать', ['class' => 'btn btn-success', 'id' => 'btn-generate-schema']) ?>
    </div>

    <div id="exists-schema">
        <?= $form->field($model, 'layoutId')->widget(Select2::className(), [
            'name' => 'layoutId',
            'data' => \yii\helpers\ArrayHelper::map(\app\models\HallLayout::find()->all(), 'id', 'layout'),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['placeholder' => 'Выберите схему', 'autocomplete' => 'off'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div id="schema">

    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
