<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HallSearch */
/* @var $hallsDataProvider yii\data\ActiveDataProvider */

$this->title = 'Залы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hall-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить зал', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'halls', 'enablePushState' => false,]); ?>

        <?= GridView::widget([
            'dataProvider' => $hallsDataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'headerOptions' => ['width' => '150']
                ],
                'number',
                [
                    'attribute' => 'status',
                    'value' => function ($hall) {
                        if ($hall->status == 0) {
                            return 'Активный';
                        } elseif ($hall->status == 1) {
                            return 'Отключен';
                        }
                        return 'Удален';
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        [
                            '0' => 'Активный',
                            '1' => 'Отключен',
                            '2' => 'Удален'
                        ],
                        [
                            'class' => 'form-control',
                            'prompt' => 'Выбрать'
                        ]
                    ),
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{view} &nbsp {update}',
                    'headerOptions' => ['width' => '80'],
                ],
            ],
        ]); ?>

    <?php Pjax::end(); ?>

</div>
