<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $hall app\models\Hall */

$this->title = 'Зал № ' . $hall->number;
$this->params['breadcrumbs'][] = ['label' => 'Залы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="hall-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $hall->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $hall,
        'attributes' => [
            'id',
            'number',
            [
                'attribute' => 'status',
                'value' => function ($hall) {
                    if ($hall->status == 0) {
                        return 'Активный';
                    } elseif ($hall->status == 1) {
                        return 'Отключен';
                    }
                    return 'Удален';
                },
            ],
            [
                'attribute' => 'hall_layout_id',
                'value' => function ($hall) {
                    return $hall->layout->layout;
                }
            ]
        ],
    ]) ?>

    <h3>Схема зала</h3>
    <div id="schema">
        <?php
            $offSeats = explode(';', $hall->layout->offSeats);

            for ($i = 1; $i < $hall->layout->rows + 1; $i++) :
        ?>
            <div class="row" style="margin: 10px 0;">
                <div class="row-label col-lg-1" style="line-height: 40px;">
                    <span ><?= "$i ряд" ?></span>
                </div>
                <div class="col-lg-11">
                    <?php
                    for ($j = 1; $j < $hall->layout->seats + 1; $j++) {
                        $seat = "$i,$j";
                        $skip = false;
                        foreach ($offSeats as $key => $offSeat) {
                            if ($offSeat == $seat) {

                                echo Html::button($j, ['class' => 'btn-seat', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px; opacity: 0;']);
                                unset($offSeats[$key]);
                                $skip = true;
                                break;
                            }
                        }
                        if (!$skip) {
                            echo Html::button($j, ['class' => 'btn-seat', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px;']);
                        }
                    }
                    ?>
                </div>
            </div>
        <?php endfor; ?>
    </div>

</div>
