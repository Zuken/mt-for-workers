<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Hall */

$this->title = 'Создание зала';
$this->params['breadcrumbs'][] = ['label' => 'Залы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hall-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
